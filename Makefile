VPATH=${GROUPHOME}/data
DB:=${GROUPHOME}/resources/snps.db
TBDRMdb=${GROUPHOME}/resources/TBDRMdb
DRUGS:=inh rif fq amk cap kan pza
RDR:=rif fq
DIRECTORIES:=$(addprefix data/,$(DRUGS))
STATES:= counts explained unexplained
export
.ONESHELL:

define strip_comments =
grep -v -e '^#' -e '^$$'
endef

all: $(DRUGS)

$(DIRECTORIES):
	mkdir $@
	for state in $(STATES); do
	  mkdir $@/$${state};
	done

data/%/known-mutations: known-mutations/%-additions.tsv | data/%
        # Ignore errors if no results are found here; The mutations file may
        # contain just a list of genes
	-$(strip_comments) $< | grep -P "\t" > $@

#data/%/known-mutations: known-mutations/%-additions.tsv | data/%
#	case $* in
#	amk|cap|kan )
#	  tbdrm_drug=ami;
#	  ;;
#	fq)
#	  tbdrm_drug=flq;
#	  ;;
#	*)
#	  tbdrm_drug=$*;
#	esac;
#	tail -n +2 $(TBDRMdb) | \
#	awk -v drug="$$tbdrm_drug" -F '\t' \
#	'BEGIN{OFS="\t";} $$4==toupper(drug) {print $$2,$$11,$$12,$$13,$$14}' | \
#	cat $< - > $@

data/%/known-genes: known-mutations/%-additions.tsv | data/%
	cut -f 1 $< | $(strip_comments) | sort | uniq > $@

$(DRUGS): %: data/%/known-mutations data/%/known-genes | data/%
	$(MAKE) -C data $@

clean:
	cd data && rm -rf $(DRUGS)

.PHONY: clean $(DRUGS)
