import csv
from KnownMutations import known_mutations_func as km


def explainedR_func(input_drug_name, input_drug_dict, variants_dict):
    if 'ofx' in input_drug_name or 'mox' in input_drug_name:
        known_mutations_file = 'known-mutations/fq-additions.tsv'
    else:
        known_mutations_file = 'known-mutations/' + input_drug_name + '-additions.tsv'
    known_mutations_list = km(known_mutations_file)
    dictionary = {}
    for isolate, mutations in variants_dict.iteritems():
        if 'pza' in input_drug_name:
            temp_mutation_list = []
            for mutation in mutations:
                if 'pncA' in mutation[0] and input_drug_dict[isolate] == 'R':
                    temp_mutation_list.append(mutation)
            if input_drug_dict[isolate] == 'R' and temp_mutation_list:
                if len(temp_mutation_list) == 1 and 'Ser65Ser' in temp_mutation_list:
                    continue
                dictionary[isolate] = temp_mutation_list
        else:
            if any(mutation for mutation in mutations if mutation[1] in known_mutations_list) and \
                            input_drug_dict[isolate] == 'R':
                for mutation in mutations:
                    if mutation[1] in known_mutations_list:
                        if isolate in dictionary.keys():
                            dictionary[isolate].append(mutation)
                        else:
                            dictionary[isolate] = [mutation]
            else:
                continue
    return dictionary


def unexplainedS_func(input_drug_name, input_drug_dict, variants_dict):
    if 'ofx' in input_drug_name or 'mox' in input_drug_name:
        known_mutations_file = 'known-mutations/fq-additions.tsv'
    else:
        known_mutations_file = 'known-mutations/' + input_drug_name + '-additions.tsv'
    known_mutations_list = km(known_mutations_file)
    dictionary = {}
    for isolate, mutations in variants_dict.iteritems():
        if 'pza' in input_drug_name:
            temp_mutation_list = []
            for mutation in mutations:
                if 'pncA' in mutation[0] and input_drug_dict[isolate] == 'S':
                    temp_mutation_list.append(mutation)
            if input_drug_dict[isolate] == 'S' and temp_mutation_list:
                dictionary[isolate] = temp_mutation_list
        else:
            # If the isolate has a known mutation
            if any(mutation for mutation in mutations if mutation[1] in known_mutations_list) and \
                            input_drug_dict[isolate] == 'S':
                for mutation in mutations:
                    if mutation[1] in known_mutations_list:
                        if isolate in dictionary.keys():
                            dictionary[isolate].append(mutation)
                        else:
                            dictionary[isolate] = [mutation]
            else:
                continue
    return dictionary


def unexplainedR_func(input_drug_name, input_drug_dict, variants_dict):
    if 'ofx' in input_drug_name or 'mox' in input_drug_name:
        known_mutations_file = 'known-mutations/fq-additions.tsv'
    else:
        known_mutations_file = 'known-mutations/' + input_drug_name + '-additions.tsv'
    known_mutations_list = km(known_mutations_file)
    unexplained_isolates = {}
    for isolate, mutations in variants_dict.iteritems():

        if 'pza' in input_drug_name:
            if (not any(mutation for mutation in mutations if 'pncA' in mutation) and input_drug_dict[isolate] == 'R') \
                    or (len(mutations) == 1 and 'Ser65Ser' in mutations[0]):
                unexplained_isolates[isolate] = ''
        else:
            try:
                if not mutations and input_drug_dict[isolate] == 'R':
                    unexplained_isolates[isolate] = ''
                elif (not any(mutation for mutation in mutations if mutation[1] in known_mutations_list) or
                        'No mutations' in mutations) and input_drug_dict[isolate] == 'R':
                    unexplained_isolates[isolate] = ''
                else:
                    continue
            except KeyError:
                continue
    return unexplained_isolates


def explainedS_func(input_drug_name, input_drug_dict, variants_dict):
    if 'ofx' in input_drug_name or 'mox' in input_drug_name:
        known_mutations_file = 'known-mutations/fq-additions.tsv'
    else:
        known_mutations_file = 'known-mutations/' + input_drug_name + '-additions.tsv'
    known_mutations_list = km(known_mutations_file)
    dictionary = {}
    for isolate, mutations in variants_dict.iteritems():
        if 'pza' in input_drug_name:
            if not any(mutation for mutation in mutations if 'pncA' in mutation) and input_drug_dict[isolate] == 'S':
                    dictionary[isolate] = ''
        else:
            if not mutations and input_drug_dict[isolate] == 'S':
                dictionary[isolate] = ''
            else:
                if (not any(mutation for mutation in mutations if mutation[1] in known_mutations_list) or
                      'No mutations' in mutations) and input_drug_dict[isolate] == 'S':
                    dictionary[isolate] = mutations
                else:
                    continue
    return dictionary


def explainedR_writer(dictionary, datadir):
    filename = datadir + '/explained-r'
    writer = csv.writer(open(filename, 'w'), delimiter='\t', lineterminator='\n')
    for key, value in dictionary.iteritems():
        if len(value) > 1:
            for mutation in value:
                writer.writerow([key, mutation[1]])
        else:
            writer.writerow([key, value[0][1]])


def unexplainedS_writer(dictionary, datadir):
    filename = datadir + '/unexplained-s'
    writer = csv.writer(open(filename, 'w'), delimiter='\t', lineterminator='\n')
    for key, value in dictionary.iteritems():
        if len(value) > 1:
            for mutation in value:
                writer.writerow([key, mutation[1]])
        else:
            writer.writerow([key, value[0][1]])


def explainedS_writer(isolates, datadir):
    explainedS_writer = csv.writer(open(datadir + '/explained-s', 'w'), delimiter='\t',
                                   lineterminator='\n')
    for isolate in isolates:
        explainedS_writer.writerow([isolate])


def unexplainedR_writer(isolates, datadir):
    unexplainedR_writer = csv.writer(open(datadir + '/unexplained-r', 'w'), delimiter='\t',
                                     lineterminator='\n')
    for isolate in isolates:
        unexplainedR_writer.writerow([isolate])

