#!/usr/bin/enc python2.7

import os
import sys
import subprocess

GROUPHOME = os.environ['GROUPHOME']


def create_bam(isolate, drug):

        bam = isolate + '.' + drug + '.region.bam'
        cram_file = GROUPHOME + '/data/depot/public-genomes-reference/' + isolate + '.cram'
        # Samtools commands
        convert2bam = ['samtools', 'view',
                       '-T', GROUPHOME + '/resources/H37Rv.fasta',
                       '-L', drug + '.bed',
                       '-b',
                       '-h',
                       '-o', bam,
                       cram_file]
        indexbam = ['samtools', 'index', bam]

        # Execute commands
        convert_process = subprocess.Popen(convert2bam)
        # Waiting for the conversion to finish
        convert_process.wait()
        index_process = subprocess.Popen(indexbam)
        # Waiting for the indexing process to finish
        index_process.wait()
        return bam


if __name__ == '__main__':
    isolate = sys.argv[1]
    drug = sys.argv[2]
    create_bam(isolate, drug)
