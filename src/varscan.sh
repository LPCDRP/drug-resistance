#!/usr/bin/env bash

cram=$1
positions=$2

varscan=/home/sbusby/Downloads/VarScanv2.3.9.jar
cram_dir=$GROUPHOME/data/depot/public-genomes-reference
H37Rv=$GROUPHOME/resources/H37Rv.fasta

grep -wf $positions \
        <(samtools view --output-fmt BAM $cram | \
          samtools mpileup -q 20 -f $H37Rv - 2> /dev/null | \
          awk -F'\t' '{OFS="\t"}{if($4 != 0){print $0}}' | \
          java -jar $varscan mpileup2cns --min-avg-qual 20 --min-var-freq 0.0 --min-coverage 1 --min-reads2 0 --strand-filter 0 --mpileup 1 2> /dev/null | \
          cut -f2,3,4,5 | sed 's/:/\t/g' | cut -f1,2,3,5,6,7)
