
.SECONDARY:
.DELETE_ON_ERROR:

drugs ?= mox ofx
bed ?= /home/sbusby/workspace/drug-papers/fluoroquinolones.pub/data/fq.bed

isolate-based-mutations-$(drugs): $(bed)
	./isolate-based-mutations.py --bed-file $< | ./add-lineage-dst-isolatebased.py -d $(drugs)
