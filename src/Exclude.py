
import pickle
import logging


def exclude_isolates(output_directory, input_drug):
    logger = logging.getLogger('FindLowCoverage')
    logger.info('Finding isolates to exclude')
    with open(output_directory + '_coverage.pickle', 'r') as pickle_file:
        coverage_dict = pickle.load(pickle_file)
    output_name = output_directory + 'excluded'
    pnca_positions = [p for p in range(2288661, 2289282)]
    known_genome_positions_file = 'data/genome-positions-canonical-mutations'
    known_positions = []
    with open(known_genome_positions_file, 'r') as f:
        for line in f:
            column = line.rstrip('\n').split('\t')
            genome_position = int(column[1])
            known_positions.append(genome_position)
    exclude = set()
    for isolate, list_of_genes in coverage_dict.iteritems():
        for list_of_positions in list_of_genes:
            if 'pza' in input_drug:
                low_positions = [pos for pos in list_of_positions[1:] if 'None' not in pos and
                                 int(pos) in pnca_positions]
            else:
                low_positions = [pos for pos in list_of_positions[1:] if 'None' not in pos and
                                 int(pos) in known_positions]
            if low_positions:
                exclude.add(isolate)

    with open(output_name, 'w') as output_file:
        for isolate in sorted(exclude):
            output_file.write(isolate + '\n')
    # Removing duplicate entries
    return sorted(list(set(sorted(exclude))))
