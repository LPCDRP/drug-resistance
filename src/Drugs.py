import os

GROUPHOME = os.environ['GROUPHOME']


def dst(drug_file, drug):
    output_drug = {}
    for file in drug_file:
        with open(file, 'r') as f:
            for line in f:
                column = line.rstrip('\n').split('\t')
                if 'isolate' in column[0]:
                    drug_column = column
                    continue
                output_drug[column[0]] = column[drug_column.index(drug)]
    return output_drug
