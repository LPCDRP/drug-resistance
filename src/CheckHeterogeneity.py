from collections import OrderedDict
import os
import pysam
import re
from Bio import SeqIO
from Bio.Blast.Applications import NcbiblastnCommandline
import logging
from multiprocessing import Pool
from functools import partial

GROUPHOME = os.environ['GROUPHOME']
logger = logging.getLogger('Heterogeneity')


def bam_coverage(bam, position, chrom):
    base_qualities = {}
    base_calls = {}
    for read in bam.fetch(chrom, position - 1, position):
        reference_positions = read.get_reference_positions(full_length=True)
        if position - 1 in reference_positions:
            index = reference_positions.index(position - 1)
            base = read.query_sequence[index]
            quality = read.query_qualities[index]
            if quality > 0:
                if base not in base_qualities.keys():
                    base_qualities[base] = [quality]
                else:
                    base_qualities[base].append(quality)

                if base not in base_calls.keys():
                    base_calls[base] = 1
                else:
                    base_calls[base] += 1
    return base_calls


def pbhoover(isolate, chromosome, region):
    output = []
    vcf = pysam.VariantFile(GROUPHOME + '/data/variants/pbhoover/' + isolate + '.vcf.gz')
    bam_file = pysam.AlignmentFile(GROUPHOME + '/data/depot/pbhoover/' + isolate + '/aligned_reads.corrected.bam')
    for position in region:
        position = int(position)
        base_calls = bam_coverage(bam_file, position, chromosome)
        for record in vcf.fetch('1', position-10, position + 10):
            vcf_filter = ''.join(record.filter)
            if 'HETERO' in vcf_filter and record.pos == position:
                isolate = isolate + 'H'
        total_coverage = 0
        base_columns = OrderedDict({'A': 0, 'T': 0, 'G': 0, 'C': 0, 'INDEL': 0})
        for base, coverage in base_calls.iteritems():
            total_coverage += coverage
            try:
                base_columns[base] = coverage
            except KeyError:
                base_columns['INDEL'] = coverage
        try:
            frequencies = OrderedDict({base: str(round(c / total_coverage, 3)) for base, c in base_columns.iteritems()})
            out = ['\t'.join([str(c), frequencies[base]]) for base, c in base_columns.iteritems()]
            output.append('\t'.join([isolate, str(position), str(int(total_coverage)), '\t'.join(out)]) + '\n')
        # Except of there is 0 total depth/coverage
        except ZeroDivisionError:
            continue
    return output


def denovo(isolate_id):
    denovo_files = [f.split('.')[0] for f in os.listdir(GROUPHOME + '/data/variants/mummer-denovo/')
                    if f.endswith('.vcf.gz')]
    if isolate_id in denovo_files:
        return True
    else:
        return False


def get_denovo_region(isolate, desired_position):
    def seq(isolate):
        sequence = []
        for record in SeqIO.parse(GROUPHOME + '/data/genomes/' + isolate + '.fasta', 'fasta'):
            sequence.append(record.seq)
        return ''.join(sequence[0])

    def align(isolate, ref, target_position):
        position1 = target_position - 100
        position2 = target_position + 100
        isolate_seq = seq(isolate)
        blast_command = NcbiblastnCommandline(cmd='blastn', task='megablast',
                                              subject=ref, outfmt=6,
                                              subject_loc=str(position1) + '-' + str(position2))
        stdout, stderr = blast_command(stdin=isolate_seq)
        qstart = stdout.rstrip('\n').split('\t')[6]
        qstop = stdout.rstrip('\n').split('\t')[7]
        qseqs = [p for p in range(int(qstart), int(qstop) + 1)]
        sseqs = [p for p in range(int(position1), int(position2) + 1)]
        combined_positions = zip(sseqs, qseqs)
        for tup in combined_positions:
            if target_position == tup[0]:
                return tup[1]
    return align(isolate, GROUPHOME + '/resources/H37Rv.fasta', int(desired_position))


def base_coverage(isolate_id, bam, chrom, region):
    output = []
    for position in region:
        base_qualities = {}
        base_calls = {}
        bam_position = position - 1
        if position in region:
            for read in bam.fetch(chrom, bam_position, bam_position + 1):
                reference_positions = read.get_reference_positions(full_length=True)
                if bam_position in reference_positions:
                    index = reference_positions.index(bam_position)
                    base = read.query_sequence[index]
                    quality = read.query_qualities[index]
                    if base not in base_qualities.keys():
                        base_qualities[base] = [quality]
                    else:
                        base_qualities[base].append(quality)

                    if base not in base_calls.keys():
                        base_calls[base] = 1
                    else:
                        base_calls[base] += 1
            total_coverage = 0.0
            base_columns = OrderedDict({'A': 0, 'T': 0, 'G': 0, 'C': 0, 'INDEL': 0})
            for base, coverage in base_calls.iteritems():
                total_coverage += coverage
                try:
                    base_columns[base] = coverage
                except KeyError:
                    base_columns['INDEL'] = coverage
            frequencies = OrderedDict({base: str(round(c / total_coverage, 3)) for base, c in base_columns.iteritems()})
            out = ['\t'.join([str(c), frequencies[base]]) for base, c in base_columns.iteritems()]
            output.append('\t'.join([isolate_id, str(position),  str(int(total_coverage)), '\t'.join(out)]) + '\n')
    return output


def illumina(isolate, positions):
    output = []
    consensus_dir = GROUPHOME + '/data/variants/public-genomes/consensus/'
    c = consensus_dir + isolate + '.consensus'
    with open(c, 'r') as f:
        for line in f:
            column = line.rstrip('\n').split('\t')
            position = column[0]
            if position in positions:
                base_columns = OrderedDict({'A': 0, 'T': 0, 'G': 0, 'C': 0, 'INDEL': 0})
                ref = column[1]
                if column[2] == '.':
                    alt = ref
                else:
                    alt = column[2]
                total_depth = float(column[3])
                ref_reads = column[4]
                alt_reads = column[5]
                # If it's not an indel
                if ref != '.' and len(alt) == 1:
                    base_columns[ref] += int(ref_reads)
                    base_columns[alt] += int(alt_reads)
                else:
                    base_columns['INDEL'] += int(alt_reads)
                frequencies = OrderedDict({base: str(round(cov / total_depth, 3))
                                           for base, cov in base_columns.iteritems()})
                out = ['\t'.join([str(cov), frequencies[base]]) for base, cov in base_columns.iteritems()]
                output.append('\t'.join([isolate, str(position),  str(int(total_depth)), '\t'.join(out)])
                              + '\n')
    return output


def writer(out_list, outd):
    with open(outd + 'heterogeneity.tsv', 'w') as output:
        output.write('\t'.join(['Isolate', 'Position', 'Depth', 'A', 'A_Frequency',
                                'T', 'T_Frequency', 'C', 'C_Frequency', 'G', 'G_Frequency', 'INDELs',
                                'INDELs_Frequency']) + '\n')
        for i in out_list:
            for j in i:
                output.write(j)


def isolate_loop(isolate, in_position_list):
    logger.debug('Checking ' + isolate)
    region = in_position_list
    if denovo(isolate):
        if 'error_correction3' in os.listdir(GROUPHOME + '/data/depot/assembly/' + isolate):
            bam = pysam.AlignmentFile(GROUPHOME + '/data/depot/assembly/' + isolate +
                                      '/error_correction3/data/aligned_reads.bam', mode='r')
        elif 'methylation' in os.listdir(GROUPHOME + '/data/depot/assembly/' + isolate):
            bam = pysam.AlignmentFile(GROUPHOME + '/data/depot/assembly/' + isolate +
                                      '/methylation/data/aligned_reads.bam', mode='r')
        # tid = The target id. The target id is 0 or a positive integer mapping to entries within the sequence
        # dictionary in the header section of a TAM file or BAM file.
        chrom = bam.get_reference_name(0)
        denovo_region = {}
        for p in region:
            denovo_region[get_denovo_region(isolate, p)] = p
        uncorrected_outputs = base_coverage(isolate, bam, chrom, denovo_region.keys())
        outputs = []
        for i in uncorrected_outputs:
            l = i.rstrip('\n').split('\t')
            denovo_pos = l[1]
            l[1] = denovo_region[int(denovo_pos)]
            outputs.append('\t'.join(l) + '\n')
    # PBHoover
    elif re.search('[0-9]-[0-9]{4}', isolate) or isolate.startswith('SEA') or isolate.startswith('R') or \
            isolate.startswith('SAWC') or isolate.startswith('SGT'):
        if isolate.startswith('R') or isolate.startswith('SAWC') or isolate.startswith('SGT'):
            chrom = '1'
        else:
            chrom = 'gi|448814763|ref|NC_000962.3|'
        outputs = pbhoover(isolate, chrom, region)
    else:
        outputs = illumina(isolate, in_position_list)
    return outputs


def check_heterogeneity(positions, isolates, out_dir, nproc):
    logger.info('Checking heterogeneity in ' + ','.join(positions))
    pool = Pool(nproc)
    partial_loop = partial(isolate_loop, in_position_list=positions)
    outputs = pool.map(partial_loop, isolates)
    writer(outputs, out_dir)
