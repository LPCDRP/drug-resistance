#!/usr/bin/env python2.7

import errno
from shutil import rmtree
from argparse import ArgumentParser, SUPPRESS
import os
import logging

import Exclude
from RetrieveVariants import all_isolates as retrievevariants
from Drugs import dst
from VariantWriter import variant_writer
import Explanation
from CoverageCheck import coverage_checker, public_genomes_coverage, egypt_sa_coverage
from CheckHeterogeneity import check_heterogeneity

GROUPHOME = os.environ['GROUPHOME']


def main():
    parser = ArgumentParser(description="""Performs the association between drug and variants in genes 
                                           known to cause resistance.""")
    parser.add_argument('-d', '--drug', help='input drug rif, amk, cap, kan, mox, ofx, pza.',
                        choices=['rif', 'inh', 'amk', 'cap', 'kan', 'mox', 'ofx', 'pza', 'fq', 'str', 'emb'])
    parser.add_argument('-c', '--clean', help='"all" argument will clean entire data/association directory or '
                                              'indicating a drug name will remove only that folder within '
                                              'data/association', default='all', nargs='+',
                        choices=['rif', 'inh', 'amk', 'cap', 'kan', 'mox', 'ofx', 'pza', 'fq', 'str', 'emb'])
    parser.add_argument('-f', '--fofn', help='FOFN of full-path VCF file names to process. Must be compressed and'
                                             ' tabix-ed VCFs and must be full-path')
    parser.add_argument('-o', '--outdir', help='Output directory. Default is data/assocation/',
                        default='data/association/')
    parser.add_argument('--hetero', help='Check a given set of positions for heterogeneous populations. Output '
                                         'is heterogeneity.tsv. Multiple positions can be check, list '
                                         'them as space-separated', nargs='+', required=False, dest='POSITIONS')
    parser.add_argument('-n', '--nproc', help='Number of processors to use when checking for heterogeneity, '
                                              'Default is 4.',
                        type=int, default=4)
    parser.add_argument('--no-denovo', dest='denovo', help=SUPPRESS, action='store_true')
    parser.add_argument('--debug', help=SUPPRESS, action='store_true')
    args = parser.parse_args()
    drug_names = ['inh', 'rif', 'amk', 'cap', 'kan', 'fq', 'mox', 'ofx', 'pza', 'str', 'emb']
    donot_use_denovo = args.denovo

    if args.debug:
        logging.basicConfig(level=logging.DEBUG,
                            format='%(asctime)s:%(levelname)s:%(name)s:%(message)s')
    else:
        logging.basicConfig(level=logging.INFO,
                            format='%(asctime)s:%(levelname)s:%(name)s:%(message)s')
    logger = logging.getLogger(__name__)

    all_dst = [GROUPHOME + '/metadata/dst.txt']

    if not args.drug:
        try:
            logger.info('Removed data/association/' + ','.join(args.clean))
            for d in args.clean:
                rmtree('data/association/' + d)
            exit()
        except OSError:
            logger.info('data/association/' + ','.join(args.clean) + '/' + ' is removed.')
            exit()
    elif ('all' in args.clean[0].lower() or 'a' in args.clean) and not args.drug:
        try:
            logger.info('Removed data/association/' + '[' + ','.join(os.listdir('data/association')) + ']')
            rmtree('data/association')
            exit()
        except OSError:
            logger.info('data/association/ is removed.')
            exit()
    elif args.drug:

        drug = args.drug
        if args.outdir == 'data/association/':
            data_dir = 'data/association/' + drug + '/'

            cwd = os.getcwd()
            folders = cwd.split('/')
            if folders[-1] != 'drug-resistance':
                logger.error('You are not in the correct directory to execute known-resistance-association.py.'
                             'Please traverse to drug_resistance/ to execute code OR provide the -o/--outdir '
                             'argument.')
                exit()
        else:
            data_dir = args.outdir

        if drug in drug_names:
            drug_dict = dst(all_dst, drug)
        else:
            logger.error('Drug not available. '
                         'Please input inh, rif, amk, kan, cap, mox, ofx, or pza or '
                         'clean the data/association directory with clean.')
            exit()

        try:
            os.makedirs(data_dir)
        except OSError as exc:
            if exc.errno != errno.EEXIST:
                raise exc
            pass
        single_isolate_file = data_dir + '/isolate-mutation-profiles'
        single_gene_file = data_dir + '/isolate-single-gene-mutation-profiles'
        single_mutation_file = data_dir + '/isolate-single-mutation-mutation-profiles'

        if os.listdir(data_dir):
            logger.warning('All files are already generated. If you would like to overwrite all files, run: \n\n'
                           '\tsrc/known-resistance-association.py --clean ' + drug + '\n\nStill running...')
        vcf_files = []
        with open(args.fofn, 'r') as f:
            for line in f:
                vcf_files.append(line.rstrip('\n'))
        logger.info('Retrieving variants for ' + args.fofn)
        variants = retrievevariants(drug, data_dir, vcf_files)
        for isolate, iso_variants in variants.copy().iteritems():
            if isolate in drug_dict.keys() and drug_dict[isolate] not in ['R', 'S']:
                variants.pop(isolate)
            elif isolate not in drug_dict.keys():
                variants.pop(isolate)
        unexplainedR_dict = Explanation.unexplainedR_func(drug, drug_dict, variants)
        unexplainedS_dict = Explanation.unexplainedS_func(drug, drug_dict, variants)
        explainedR_dict = Explanation.explainedR_func(drug, drug_dict, variants)
        explainedS_dict = Explanation.explainedS_func(drug, drug_dict, variants)
        coverage_checker(drug, data_dir, denovo_flag=donot_use_denovo, isolate=unexplainedR_dict.keys())
        coverage_checker(drug, data_dir, denovo_flag=donot_use_denovo, isolate=explainedS_dict.keys())
        public_genomes_coverage(drug, data_dir, 'unexplained-r-coverage', unexplainedR_dict.keys(), args.nproc)
        public_genomes_coverage(drug, data_dir, 'explained-s-coverage', explainedS_dict.keys(), args.nproc)
        excluded_isolates = Exclude.exclude_isolates(data_dir, drug)
        for isolate in excluded_isolates:
            variants.pop(isolate)
        # Detecting heterogeneity in unexplained-r with excluded removed
        if args.POSITIONS:
            check_heterogeneity(args.POSITIONS,
                                sorted(list(set(sorted(unexplainedR_dict.keys())) - set(sorted(excluded_isolates)))),
                                data_dir,
                                args.nproc)
        # Writing files
        variant_writer(variants, single_isolate_file, single_gene_file, single_mutation_file)
        Explanation.explainedR_writer(explainedR_dict, data_dir)
        # Removing excluded isolates from explained-s and unexplained-r
        Explanation.explainedS_writer(sorted(list(set(sorted(explainedS_dict.keys())) -
                                                  set(sorted(excluded_isolates)))),
                                      data_dir)
        Explanation.unexplainedR_writer(sorted(list(set(sorted(unexplainedR_dict.keys())) -
                                                    set(sorted(excluded_isolates)))),
                                        data_dir)
        Explanation.unexplainedS_writer(unexplainedS_dict, data_dir)
        logger.info('Output located in ' + data_dir)


if __name__ == '__main__':
    main()
