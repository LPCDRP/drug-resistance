import os
import pysam
import re
import pickle
import logging

GROUPHOME = os.environ['GROUPHOME']
gene_dict = {}
with open('data/genes.tsv', 'r') as genes:
    for line in genes:
        column = line.rstrip('\n').split('\t')
        gene_dict[column[0]] = [int(column[1]), int(column[2])]
gene_drug_dict = {}
with open('data/genes-drug.tsv', 'r') as genes:
    for line in genes:
        column = line.rstrip('\n').split('\t')
        gene_drug_dict[column[0]] = column[1:]


def egypt_sa_coverage(input_drug, output, isolate_list):
    output_dict = {}
    for isolate in isolate_list:
        bam_dir = GROUPHOME + '/data/depot/egypt-saudiarabia-reference/'
        extension = '.bam'
        bam = bam_dir + isolate + extension
        for gene in gene_drug_dict[input_drug]:
            start = gene_dict[gene][0]
            stop = gene_dict[gene][1]
            if start > stop:
                start = stop
                stop = start
            else:
                start = start
                stop = stop
            try:
                coverage = pysam.depth('-a',
                                       '-r',
                                       '1:' + str(start) + '-' +
                                       str(stop),
                                       bam).split('\n')
                for row in coverage:
                    if row:
                        depth = int(row.split('\t')[2])
                        pos = row.split('\t')[1]
                        if depth <= 10:
                            if isolate in output_dict.keys():
                                output_dict[isolate].append(pos)
                            else:
                                output_dict[isolate] = [pos]

            except pysam.utils.SamtoolsError:
                continue
    mode = 'w'
    for isolate, position in output_dict.iteritems():
            with open(output, mode) as output_filehandle:
                output_filehandle.write('\t'.join([isolate] + position) + '\n')


def public_genomes_coverage(input_drug, data_directory, output, isolate_list, nproc):
    """Checks coverage for the public genomes. If the depth is less than 10, it is considered low"""
    logger = logging.getLogger('IlluminaGenomesCoverage')
    pickle_file = '_coverage.pickle'
    if pickle_file in os.listdir(data_directory):
        logger.debug('Loading pickled dictionary')
        with open(data_directory + pickle_file, 'r') as pickle_handle:
            output_dict = pickle.load(pickle_handle)
    else:
        output_dict = {}
    if len(isolate_list) == len(output_dict.keys()):
        with open(data_directory + pickle_file, 'w') as pickle_handle:
            pickle.dump(output_dict, pickle_handle)
    else:
        updated_isolates = list(set(sorted(isolate_list)) - set(sorted(output_dict.keys())))
        logger.info('Checking coverage for ' + '-'.join(output.split('-')[0:2]))
        logger.debug('Identifying low coverage positions')
        missing_consensus = []
        for isolate in updated_isolates:
            if re.search('[0-9]-[0-9]{4}', isolate) or isolate.startswith('SEA') or isolate.startswith('R') or \
                    isolate.startswith('SAWC') or isolate.startswith('SGT'):
                continue
            consensus_file = GROUPHOME + '/data/variants/public-genomes/consensus/' + isolate + '.consensus'
            try:
                consensus_dict = {}
                with open(consensus_file, 'r') as f:
                    for line in f:
                        column = line.rstrip('\n').split('\t')
                        position = int(column[0])
                        total_depth = int(column[3])
                        consensus_dict[position] = total_depth
                gene_pos_list = []
                for gene in gene_drug_dict[input_drug]:
                    gene_pos_list = [gene]
                    start = gene_dict[gene][0]
                    stop = gene_dict[gene][1]
                    if start > stop:
                        start = stop
                        stop = start
                    else:
                        start = start
                        stop = stop
                    for p in range(int(start), int(stop) + 1):
                        # If the position is not in the consensus file, the depth is 0
                        try:
                            depth = consensus_dict[p]
                        except KeyError:
                            depth = 0
                        if depth <= 10:
                            gene_pos_list.append(str(p))
                    if len(gene_pos_list) == 1:
                        gene_pos_list = [gene, 'None']
                if isolate in output_dict.keys():
                    output_dict[isolate].append(gene_pos_list)
                else:
                    output_dict[isolate] = [gene_pos_list]
            except OSError:
                missing_consensus.append(isolate)
                continue
        if missing_consensus:
            logger.warning('Report the following isolate(s) to https://gitlab.com/LPCDRP/drug-resistance/issues '
                           '(Assignee: Sarah) as not having a consensus file: ' + '\n'.join(missing_consensus))
        with open(data_directory + pickle_file, 'w') as pickle_handle:
            pickle.dump(output_dict, pickle_handle)


def coverage_checker(input_drug, data_directory, denovo_flag, isolate='all'):
    """Checks for LOWs in the PBHoover debug infile
    and 0 depth in the coverage.txt files. Default is all isolates.
    Can insert string or list of isolates."""
    logger = logging.getLogger('PBHooverGenomesCoverage')
    logger.info('Checking coverage')
    # if os.path.isfile('data/association/' + input_drug + '/' + output):
    #     mode = 'a'
    # else:
    mode = 'w'
    # coverage_writer = csv.writer(open(data_directory + output, mode), delimiter='\t')
    denovo_isolates = [i.split('.')[0] for i in os.listdir(GROUPHOME + '/data/variants/mummer-denovo/')
                       if i.endswith('vcf.gz')]
    if 'all' in isolate:
        pickle_file = data_directory + '_coverage.pickle'
        if '_coverage.pickle' in os.listdir(data_directory):
            with open(pickle_file, 'r') as f:
                coverage_dict = pickle.load(f)
        else:
            coverage_dict = {}
        for infile in os.listdir(GROUPHOME + '/data/variants/pbhoover/consensus/'):
            # if infile.split('.')[0] + '.vcf.gz' in \
            #         os.listdir(GROUPHOME + '/data/variants/mummer-denovo'):
            #     continue
            vcf_isolate = infile.split('.')[0]
            if infile.endswith('consensus.vcf.gz') and '1-0028' not in infile and not infile.startswith('A') and \
                    vcf_isolate not in denovo_isolates:
                for gene in gene_drug_dict[input_drug]:
                    start = gene_dict[gene][0]
                    stop = gene_dict[gene][1]
                    try:
                        low_dict = low(GROUPHOME + '/data/variants/pbhoover/consensus/' + infile, gene, start, stop)
                        coverage_dict.update(low_dict)
                    except IOError:
                        continue
        # for isolate_id, positions in coverage_dict.iteritems():
        #     coverage_writer.writerow([isolate_id] + positions)
        with open(pickle_file, 'w') as f:
            pickle.dump(coverage_dict, f)
    elif type(isolate) is list:
        pickle_file = data_directory + '_coverage.pickle'
        if '_coverage.pickle' in os.listdir(data_directory):
            with open(pickle_file, 'r') as f:
                coverage_dict = pickle.load(f)
        else:
            coverage_dict = {}
        pbhoover_dir = GROUPHOME + '/data/variants/pbhoover/consensus/'
        if not denovo_flag:
            for iso in isolate:
                if iso + '.consensus.vcf.gz' in os.listdir(pbhoover_dir) and iso not in denovo_isolates:
                    infile = pbhoover_dir + iso + '.consensus.vcf.gz'
                    for gene in gene_drug_dict[input_drug]:
                        start = gene_dict[gene][0]
                        stop = gene_dict[gene][1]
                        low_pos_list = low(infile, gene, start, stop)
                        if iso in coverage_dict.keys():
                            coverage_dict[iso].append(low_pos_list)
                        else:
                            coverage_dict[iso] = [low_pos_list]
        else:
            for iso in isolate:
                if iso + '.consensus.vcf.gz' in os.listdir(pbhoover_dir):
                    infile = pbhoover_dir + iso + '.consensus.vcf.gz'
                    for gene in gene_drug_dict[input_drug]:
                        start = gene_dict[gene][0]
                        stop = gene_dict[gene][1]
                        low_pos_list = low(infile, gene, start, stop)
                        if iso in coverage_dict.keys():
                            coverage_dict[iso].append(low_pos_list)
                        else:
                            coverage_dict[iso] = [low_pos_list]

        # for isolate_id, positions in coverage_dict.iteritems():
        #     coverage_writer.writerow([isolate_id] + positions)
        with open(pickle_file, 'w') as f:
            pickle.dump(coverage_dict, f)


    # For testing purposes, input is a string
    else:
        """
        # This works except that Pool.map has an issue with a gzip filehandle as iterable

        isolate_list = [isolate]
        low_pos = {}
        for input_isolate in isolate_list:
            infile = gzip.open(GROUPHOME + '/data/variants/pbhoover/' + input_isolate + '.debug.vcf.gz', 'r')
            p = Pool(4)
            new_low = partial(low, input_drug, low_pos)
            p.map(new_low, infile, 1)
            p.start()
            p.join()
        """
        infile = GROUPHOME + '/data/variants/pbhoover/' + isolate + '.consensus.vcf.gz'
        for gene in gene_drug_dict[input_drug]:
            start = gene_dict[gene][0]
            stop = gene_dict[gene][1]
            coverage_dict = low(infile, gene, start, stop)
            for isolate, positions in coverage_dict.iteritems():
                print isolate, positions


def low(input_vcf, gene, start, stop):
    """Retrieves positions with FILTER of LOW in the debug VCF file"""

    return_dict = {}
    input_vcf_split = input_vcf.split('/')
    filename = input_vcf_split[-1].split('.')
    isolate = filename[0]

    try:
        vcf_file = pysam.VariantFile(input_vcf)
    except IOError:
        blank_dict = {}
        return blank_dict

    gene_pos_list = [gene]
    for record in vcf_file.fetch('1', start, stop):
        if 'LOW' in record.filter:
            gene_pos_list.append(str(record.pos))
    if len(gene_pos_list) == 1:
        gene_pos_list = [gene, 'None']
    gene_pos_list = zero(isolate, gene_pos_list, start, stop)
    # return_dict[isolate] = gene_pos_list
    return gene_pos_list


def zero(isolate, low_positions_list, start, stop):
    """Retrieves positions with depth of zero in the coverage file"""
    bam_dir = GROUPHOME + '/data/depot/pbhoover/' + isolate
    try:
        coverage = pysam.depth('-a',
                               '-r',
                               'gi|448814763|ref|NC_000962.3|:' + str(start) + '-' +
                               str(stop),
                               bam_dir + '/aligned_reads.corrected.bam').split('\n')
        for item in coverage:
            if item:
                depth = int(item.rstrip('\n').split('\t')[2])
                pos = int(item.rstrip('\n').split('\t')[1])
                if depth == 0:
                    low_positions_list.append(str(pos))
    except pysam.utils.SamtoolsError:
        test = ''
    return low_positions_list
