#!/usr/bin/env python


def known_mutations_func(known_mutations_input):
    km_list = []
    with open(known_mutations_input, 'r') as f:
        for line in f:
            column = line.rstrip('\n').split('\t')
            km_list.append(column[1])
    return km_list
