#!/usr/bin/env python

import pysam
import os
import sys
import re
import csv
from argparse import ArgumentParser, FileType
from collections import OrderedDict
from RetrieveVariants import variants_func

GROUPHOME = os.environ['GROUPHOME']


def add_lineage_dst(dictionary, drugs, dst_file, bed, output_file):
    lineage_file = GROUPHOME + '/metadata/lineage.txt'
    # lineage_file = GROUPHOME + '/metadata/egypt-saudiarabia.lineage.tsv'

    lineage = dict()
    dst = dict()
    gene_header = []
    for gene, positions in bed.iteritems():
        gene_header.append(gene)

    if 'mox' in drugs or 'ofx' in drugs:
        output_file.write('\t'.join(['Isolate'] + gene_header + ['Calls', 'Lineage'] +
                               [drug.upper() + ' Susceptibility' for drug in drugs] + ['OFX MIC', 'MOX MIC']) + '\n')
        mic = dict()
        mic_file = GROUPHOME + '/metadata/fq-mic-concetrantions.tsv'
        with open(mic_file, 'r') as mic_filehandle:
            mic_filehandle.next()
            for line in mic_filehandle:
                column = line.rstrip('\n').split('\t')
                mic[column[0]] = [column[1], column[2]]
    else:
        output_file.write('\t'.join(['Isolate'] + gene_header + ['Calls', 'Lineage'] +
                               [drug.upper() + ' Susceptibility' for drug in drugs]) + '\n')

    lineage_filehandle = open(lineage_file, 'r')
    for drug in drugs:
        dst_filehandle = open(dst_file, 'r')
        for line in dst_filehandle:
            column = line.rstrip('\n').split('\t')
            if 'isolate' in column:
                dst_header = column
                continue
            try:
                if column[0] not in dst.keys():
                    dst[column[0]] = [column[dst_header.index(drug)]]
                else:
                    dst[column[0]].append(column[dst_header.index(drug)])
            except ValueError:
                if column[0] not in dst.keys():
                    dst[column[0]] = ['ND']
                else:
                    dst[column[0]].append('ND')
        dst_filehandle.close()

    for line in lineage_filehandle:
        column = line.rstrip('\n').split('\t')
        lineage[column[0]] = column[1]
    lineage_filehandle.close()

    for isolate in dst.keys():
        if isolate in lineage.keys() and isolate in dictionary.keys():
            mutation_calls = dictionary[isolate]
            column = [isolate] + mutation_calls
            if column[-1] == ',' or not column[-1]:
                del column[-1]
            if 'mox' in drugs or 'ofx' in drugs:
                if column[0] not in mic.keys():
                    output_mic = ['ND', 'ND']
                else:
                    output_mic = mic[column[0]]
                output_file.write('\t'.join(column + [lineage[column[0]]] + dst[column[0]] + output_mic) + '\n')
            else:
                output_file.write('\t'.join(column + [lineage[column[0]]] + dst[column[0]]) + '\n')


def create_bed_dict(bed_file):
    """Creates a dictionary that contains the gene name as key and start/stop position as a list of values"""

    bed_dict = OrderedDict()
    with open(bed_file, 'r') as bed:
        for line in bed:
            column = line.rstrip('\n').split('\t')
            bed_dict[column[0]] = [column[1], column[2]]
    return bed_dict


def get_variants(vcf, input_gene, start, stop):
    """Parses CSQ column in the VCF file to output mutations in the correct format"""
    isolate_gene_mutations = []
    gene = input_gene.split('_')[0]
    for record in vcf.fetch('1', start, stop):
        call = ''.join(record.filter.keys())
        ref_codon_var = variants_func(record, gene)
        if not ref_codon_var:
            continue
        isolate_gene_mutations.append([ref_codon_var[1], call])
    return isolate_gene_mutations


def main():
    parser = ArgumentParser(description="""Based in a BED file, it creates an isolate-based file that contains columns \
                                            with all mutations in each input gene.""")
    parser.add_argument('-i', '--isolates', help='List of isolates to analyze, one isolate per line')
    parser.add_argument('-b', '--bed-file', help='A BED file (Column1=gene name, Column2=start position, Column3=stop \
                                                position)', required=True, dest='bed_file')
    parser.add_argument('-l', '--location', help='Location of VCF files to access. By default, '
                                                 '$GROUPHOME/data/varaints/public-genomes and pbhoover is checked')
    parser.add_argument('-t', '--test', help='Indicates the use of test cases', required=False, action='store_true')
    parser.add_argument('-d', '--drug', help='Input drugs, space separated', nargs='*',
                        choices=['inh', 'rif', 'amk', 'cap', 'kan', 'ofx', 'mox', 'pza', 'str', 'emb'])
    parser.add_argument('--dst', help='Input tab-separated DST location and file name')
    parser.add_argument('-o', '--output', help='Output filename. Stdout is default', default=sys.stdout,
                        type=FileType('w'))
    args = parser.parse_args()

    if args.test:
        dst_file = 'test-dst.txt'
    else:
        dst_file = args.dst
    bed_file = args.bed_file
    drugs = list(args.drug)
    bed = create_bed_dict(bed_file)
    output_dict = OrderedDict()
    vcf_location = args.location
    isolate_list = args.isolates
    with open(isolate_list, 'r') as isolates:
        for line in isolates:
            isolate = line.rstrip('\n')
            all_calls = []
            all_mutations = []
            if 'SEA' in isolate:
                continue
            if not vcf_location:
                if isolate + '.vcf.gz' in os.listdir(GROUPHOME + '/data/variants/public-genomes/'):
                    vcf = pysam.VariantFile(GROUPHOME + '/data/variants/public-genomes/' + isolate + '.vcf.gz')
                else:
                    vcf = pysam.VariantFile(GROUPHOME + '/data/variants/pbhoover/' + isolate + '.vcf.gz')
            else:
                try:
                    vcf = pysam.VariantFile(vcf_location + '/' + isolate + '.vcf.gz')
                except IOError:
                    continue

            for input_gene, positions in bed.iteritems():
                start = int(positions[0])
                stop = int(positions[1])
                gene_variants = get_variants(vcf, input_gene, start, stop)
                mutations = []
                calls = []
                for mutation_call in gene_variants:
                    mutations.append(mutation_call[0])
                    calls.append(mutation_call[1])
                if not gene_variants:
                    mutations = ['None']
                    calls = ['None']
                all_mutations.append(','.join(mutations))
                all_calls.append(','.join(calls))

            output_dict[isolate] = all_mutations + [';'.join(all_calls)]
        add_lineage_dst(output_dict, drugs, dst_file, bed, args.output)


if __name__ == '__main__':
    main()