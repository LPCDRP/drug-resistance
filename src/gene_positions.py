#!/usr/bin/env python

import sys
import csv
import os

GROUPHOME = os.environ['GROUPHOME']
ALBORZHOME = '/home/sarah/alborz'

query_gene_file = sys.argv[1]
outputfile = sys.argv[2]
whole_genome_file = ALBORZHOME + '/resources/genome_genelists/H37Rv_wholegenome.txt'
tu_file = ALBORZHOME + '/workspace/pza-alt-mech/baughn-genes/tu.tsv'
genes_tu_file = GROUPHOME + '/resources/mtb-reconstruction/genes.tsv'
writer = csv.writer(open(outputfile, 'wb'), delimiter='\t')

genome = {}
query_genes = []
gene_tu = {}
gene_alias = {}
alias_gene = {}
tu = {}
tu_genes = {}

with open(whole_genome_file, 'r') as f:
    for line in f:
        column = line.rstrip('\n').split('\t')
        genome[column[0]] = [int(column[1]),int(column[2])]

with open(genes_tu_file, 'r') as f:
    f.next()
    for line in f:
        column = line.rstrip('\n').split('\t')
        if 'RNA' in column[2]:
            gene_tu[column[1]] = ''
        gene_tu[column[0]] = column[12]
        gene_alias[column[0]] = column[1]
        alias_gene[column[1]] = column[0]

with open(tu_file, 'r') as f:
    f.next()
    for line in f:
        column = line.rstrip('\n').split('\t')
        genes = column[1].split('-')
        gene1 = genes[0]
        gene2 = genes[1]
        #column2 = column[1].split('-')
        #gene1 = alias_gene[column2[0]]
        #gene2 = alias_gene[column2[1]]
        tu[column[0]] = [int(column[2]), int(column[3])]
        tu_genes[column[0]] = [gene1, gene2]
#print alias_gene
with open(query_gene_file, 'r') as f:
    for line in f:
        column = line.rstrip('\n').split('\t')
        ## Complement ##
        if 'c' in column[0]:
            if 'intergenic' in column[0]:
                start = genome[column[0]][0]
                stop = genome[column[0]][1]
                #print '\t'.join([column[1],str(start),str(stop)])
                writer.writerow([column[0], str(start), str(stop)])
            else:
                gene = column[0]
                # Coding #
                coding_start = genome[gene][0]
                coding_stop = genome[gene][1]
                writer.writerow([gene, str(coding_start), str(coding_stop)])
                # Promoter #
                # Gene 1
                if 'Rv' in tu_genes[gene_tu[gene]][0]:
                    promoter_gene1 = tu_genes[gene_tu[gene]][0]
                else:
                    promoter_gene1 = alias_gene[tu_genes[gene_tu[gene]][0]]
                # Gene 2
                if 'Rv' in tu_genes[gene_tu[gene]][1]:
                    promoter_gene2 = tu_genes[gene_tu[gene]][1]
                else:
                    promoter_gene2 = alias_gene[tu_genes[gene_tu[gene]][1]]

                # print promoter_gene1, promoter_gene2, gene

                promoter_start = tu[gene_tu[gene]][1]
                promoter_stop = tu[gene_tu[gene]][1] + 200
                writer.writerow([promoter_gene2 + '_prom', str(promoter_start), str(promoter_stop), gene])
        else:
            #print column
            gene = column[0]
            # Coding #
            coding_start = genome[gene][0]
            coding_stop = genome[gene][1]
            writer.writerow([column[0], str(coding_start), str(coding_stop)])
            # Promoter #
            # RNA Gene
            if '' in gene_tu[gene]:
                promoter_gene1 = gene
            elif 'Rv' in tu_genes[gene_tu[gene]][0]:
                promoter_gene1 = tu_genes[gene_tu[gene]][0]
            else:
                promoter_gene1 = alias_gene[tu_genes[gene_tu[gene]][0]]
            """
            # Gene 2
            if 'Rv' in tu_genes[gene_tu[gene]][1]:
                promoter_gene2 = tu_genes[gene_tu[gene]][1]
            else:
                promoter_gene2 = alias_gene[tu_genes[gene_tu[gene]][1]]
            """
            # print promoter_gene1, promoter_gene2, gene
            if '' in gene_tu[gene]:
                promoter_start = genome[gene][0] - 200
                promoter_stop = genome[gene][0]
            else:
                promoter_start = tu[gene_tu[gene]][0] - 200
                promoter_stop = tu[gene_tu[gene]][0]
            writer.writerow([promoter_gene1 + '_prom', str(promoter_start), str(promoter_stop), gene])
