

def variant_writer(dictionary, single_isolate_output, single_gene_output, single_mutation_output):
    output = open(single_isolate_output, 'w')
    for isolate, mutations in dictionary.iteritems():
        genes = set(item[0] for item in mutations if item is not None)
        output_list = []
        if mutations:
            for gene in genes:
                mutation_list = []
                for mutation in mutations:
                    if mutation[0] == gene:
                        mutation_list.append(mutation[1])
                output_list.append(gene + ':' + ','.join(mutation_list))
        else:
            output_list.append('No mutations')
        output.write('\t'.join([isolate, ';'.join(output_list)]) + '\n')
    single_mutation_writer = open(single_mutation_output, 'w')
    with open(single_gene_output, 'wb') as single_gene_write:
        for isolate, mutations in dictionary.iteritems():
            genes = set(item[0] for item in mutations if item is not None)
            if mutations:
                for gene in genes:
                    output_list = []
                    mutation_list = []
                    for mutation in mutations:
                        if mutation[0] == gene:
                            mutation_list.append(mutation[1])
                            single_mutation_writer.write('\t'.join([isolate, gene + ':' + mutation[1]]) + '\n')
                    output_list.append(gene + ':' + ','.join(mutation_list))
                    single_gene_write.write('\t'.join([isolate] + output_list) + '\n')
            else:
                output_list = ['No mutations']
                single_gene_write.write('\t'.join([isolate] + output_list) + '\n')
                single_mutation_writer.write('\t'.join([isolate] + output_list) + '\n')
    output.close()
    single_mutation_writer.close()
