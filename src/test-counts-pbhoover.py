#!/usr/bin/env python

import os
import unittest
from counts_pbhoover import *



class CountsTestCase(unittest.TestCase):
    """Tests for `counts-pbhoover.py`."""

    def test_basic_variant_function(self):
        test_dict = variants_func('rif','test-single-gene-output',isolate='1-0002')
        self.assertIn('Ser450Leu', test_dict.values()[0][0])

    """
    variants_30106 = variants_func(drug,'test-3-0106-single-gene',isolate='3-0106')
for isolate, mutations in variants_30106.iteritems():
    if any('No mutations' in mutation[1] and 'rpoB' in mutation[0] for mutation in mutations):
        test_results['No mutation in first or second gene'] = 'PASS'
    else:
        test_results['No mutation in first or second gene'] = 'FAILED'
        print variants_30106
    """
    def test_no_mutations(self):
        no_mutations_dict = variants_func('rif',isolate='3-0106')
        self.assertIn()


if __name__ == '__main__':
    if __name__ == '__main__':
        unittest.main()

"""

GROUPHOME = os.environ['GROUPHOME']
drug_file = GROUPHOME + '/metadata/dst.txt'
test_results = OrderedDict()
##############################################################################
## Testing PZA
drug = 'pza'

known_mutations = known_mutations_func('../known-mutations/' + drug + '-additions.tsv')
pza = pyrazinamide(drug_file)
pza_variants = variants_func(drug, 'test-pza-single-gene', isolate='1-0002')
# print pza_variants

# Testing accuracy of variant calling
for isolate, mutations in pza_variants.iteritems():
    for mutation in mutations:
        if any(mutation[1]=='Ala102Thr' for mutation in mutations):
            test_results['PZA variant calling'] = 'PASSED'
        else:
            test_results['PZA variant calling'] = 'FAILED'
            print isolate, mutations

# Testing explained resistant
pza_explainedR_variants = variants_func(drug,'test-explainedr-pza',isolate='1-0006')
pza_explainedR = explainedR_func(drug,pza,pza_variants,known_mutations)
if '1-0002' in pza_explainedR:
    test_results['PZA explained resistant'] = 'PASSED'
else:
    test_results['PZA explained resistant'] = 'FAILED'

# Testing unexplained resistant
pza_unexplainedR_variants = variants_func(drug, 'test-unexplainedr-pza',isolate='1-0019')
pza_unexplainedR = unexplainedR_func(drug,pza,pza_unexplainedR_variants,known_mutations)
if '1-0019' in pza_unexplainedR.keys():
    test_results['PZA unexplained resistant'] = 'PASSED'
else:
    test_results['PZA unexplained resistant'] = 'FAILED'
    print pza_unexplainedR

# Testing explained susceptible
pza_explainedS_variants = variants_func(drug, 'test-explaineds-pza', isolate='1-0008')
pza_explainedS = explainedS_func(drug,pza,pza_explainedS_variants,known_mutations)
if '1-0008' in pza_explainedS.keys():
    test_results['PZA explained susceptible'] = 'PASSED'
else:
    test_results['PZA explained susceptible'] = 'FAILED'

# Testing unexplained susceptible
pza_unexplainedS_variants = variants_func(drug, 'test-unexplaineds-pza', isolate='1-0002')
pza_unexplainedS = unexplainedS_func(drug, pza, pza_unexplainedS_variants,known_mutations)
if '1-0008' in pza_explainedS.keys():
    test_results['PZA unexplained susceptible'] = 'PASSED'
else:
    test_results['PZA unexplained susceptible'] = 'FAILED'



##############################################################################
fq = fluoroquinolones(drug_file)
drug = 'fq'

resistant = []
susceptible = []
for isolate, dst in fq.iteritems():
    if "R" in dst:
        resistant.append(isolate)
    elif "S" in dst:
        susceptible.append(isolate)
#print "FQ resistant: ", len(resistant)
#print 'FQ susceptible: ', len(susceptible)
##############################################################################
drug = 'rif'
rif = rifampicin(GROUPHOME + '/metadata/dst.txt')
if rif['1-0002'] == "R":
    test_results["DST"] = "PASSED"
else:
    test_results["DST"] = 'FAILED'

known_mutations = known_mutations_func('../known-mutations/' + drug + '-additions.tsv')
##############################################################################
# Isolate with no mutation in first or second gene
variants_30106 = variants_func(drug,'test-3-0106-single-gene',isolate='3-0106')
for isolate, mutations in variants_30106.iteritems():
    if any('No mutations' in mutation[1] and 'rpoB' in mutation[0] for mutation in mutations):
        test_results['No mutation in first or second gene'] = 'PASS'
    else:
        test_results['No mutation in first or second gene'] = 'FAILED'
        print variants_30106
##############################################################################
# Testing R isolate without known mutations
test_novariants = variants_func(drug, 'test-rif-single-gene-1-0016', isolate='1-0016')
# print test_variants
unexplainedR_10016 = unexplainedR_func(drug,rif,test_novariants,known_mutations)
if '1-0016' in unexplainedR_10016.keys():
    test_results['Resistant isolate without known mutation'] = 'PASSED'
else:
    test_results['Resistant isolate without known mutation'] = 'FAILED'
##############################################################################
# Testing accuracy of variants function
variants10002 = variants_func(drug, 'test-rif-single-gene', isolate='1-0002')
variants40062 = variants_func(drug, 'test-rif-40062',isolate='4-0062')
temp = []
for mutations in variants10002.values():
    for mutation in mutations:
        temp.append(mutation[1])
print variants10002

if 'Ser450Leu' in temp:
    test_results['Variants'] = 'PASSED'
else:
    test_results['Variants'] = 'FAILED'

for mutations in variants40062.itervalues():
    if any('rpoB' in mutation[0] and 'TC1349T' in mutation[1] for mutation in mutations):
        test_results['Correct nucleotide position for indels'] = 'PASSED'
    else:
        test_results['Correct nucleotide position for indels'] = 'FAILED'

##############################################################################
# Testing explained R function
explainedr_variants = variants_func(drug, 'test-explainedr-rif-single-gene', isolate='1-0002')
explainedR_dict = explainedR_func(drug, rif, explainedr_variants, known_mutations)
if '1-0002' in explainedR_dict:
    test_results['Explained Resistance'] = 'PASSED'
else:
    test_results['Explained Resistance'] = 'FAILED'
##############################################################################
# Testing unexplained R function
unexplainedr_variants = variants_func(drug, 'test-unexplainedr-rif-single-gene', isolate='1-0022')
unexplainedR_dict = unexplainedR_func(drug, rif, unexplainedr_variants, known_mutations)
if '1-0022' in unexplainedR_dict:
    test_results['Unexplained Resistance'] = 'PASSED'
else:
    test_results['Unexplained Resistance'] = 'FAILED'
##############################################################################
# Testing explained S function
explaineds_variants = variants_func(drug, 'test-explaineds-rif-single-gene', isolate='1-0160')
explainedS_dict = explainedS_func(drug, rif, explaineds_variants, known_mutations)
if '1-0160' in explainedS_dict:
    test_results['Explained Susceptible'] = 'PASSED'
else:
    test_results['Explained Susceptible'] = 'FAILED'
##############################################################################
# Testing unexplained S function
unexplaineds_variants = variants_func(drug, 'test-unexplaineds-rif-single-gene', isolate='1-0043')
unexplainedS_dict = unexplainedS_func(drug, rif, unexplaineds_variants, known_mutations)
if '1-0043' in unexplainedS_dict:
    test_results['Unexplained Susceptible'] = 'PASSED'
else:
    test_results['Unexplained Susceptible'] = 'FAILED'
##############################################################################
## Testing injectables
drug = 'kan'
kan = kanamycin(drug_file)
known_mutations = known_mutations_func('../known-mutations/kan-additions.tsv')
variants_20062 = variants_func(drug, 'test-amk',isolate='2-0062')
kan_explainedR = explainedR_func(drug, kan,variants_20062,known_mutations)

explainedR_writer(drug,kan_explainedR)
with open('../data/pbhoover/kan/explained-r', 'r') as f:
    for line in f:
        if '2-0062' in line:
            test_results['Explained R writer'] = 'PASSED'
##############################################################################
for function, result in test_results.iteritems():
    print function + ':', result
"""
