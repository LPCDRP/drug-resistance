#!/usr/bin/env python

import csv

tus_file="potential-genes-tus.txt"
gene_file="potential-genes-updated.txt"

allgenes_file="genes.tsv"

tus_only_file="potential-genes-tus-only.txt"
genes_tus_file="tus-all-columns.txt"

output=open("genes-promoter-positions.txt",'w')
writer = csv.writer(output,delimiter='\t')

tus={}
genes={}
operon={}
allgenes={}
genes_tus={}

with open(allgenes_file,'r') as f:
	for line in f:
		column=line.rstrip('\n').rstrip('\n').split('\t')
		if not column:
			continue
		else:
			allgenes[column[1]]=column[0]

with open(tus_only_file,'r') as f:
	for line in f:
		column=line.rstrip('\n').rstrip('\n').split('\t')
		genes_tus[column[1]]=column[0]

with open(tus_file,'r') as f:
	for line in f:
		column=line.rstrip('\n').split('\t')
		column2=column[1].split('-')

		if column2[0]==column2[1]:
			
			if "Rv" in column2[0]:
				
				if "c" in column2[0]:
					
					tus[column2[0]]=[int(column[3]),int(column[3])+200]
				else:
					
					tus[column2[0]]=[int(column[2])-200,int(column[2])]
			else:
				
				rvgene=allgenes[column2[0]]
				if "c" in rvgene:
					
					tus[rvgene]=[int(column[3]),int(column[3])+200]
				else:
					
					tus[rvgene]=[int(column[2])-200,int(column[2])]
		else:
			
			print genes_tus[column[0]],column2
			if "Rv" in column2[0]:
				
				if "c" in column2[0]:
					
					tus[column2[0]]=[int(column[3]),int(column[3])+200]
				else:
					
					tus[column2[0]]=[int(column[2])-200,int(column[2])]
			else:
				
				rvgene=allgenes[column2[0]]
				if "c" in rvgene:
					
					tus[rvgene]=[int(column[3]),int(column[3])+200]
				else:
					
					tus[rvgene]=[int(column[2])-200,int(column[2])]



for key,value in tus.iteritems():
	writer.writerow([key+'_prom',value[0],value[1]])






		
