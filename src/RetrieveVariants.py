import os
import logging
import pysam
from Bio.SeqUtils import seq3
from Bio.Seq import Seq
from difflib import Differ
import pickle

GROUPHOME = os.environ['GROUPHOME']

gene_dict = {}
with open('data/genes.tsv', 'r') as genes:
    for line in genes:
        column = line.rstrip('\n').split('\t')
        gene_dict[column[0]] = [int(column[1]), int(column[2])]
gene_drug_dict = {}
with open('data/genes-drug.tsv', 'r') as genes:
    for line in genes:
        column = line.rstrip('\n').split('\t')
        gene_drug_dict[column[0]] = column[1:]

logger = logging.getLogger()


def all_isolates(input_drug, out_dir, list_of_files):

    if '_variants.pickle' in os.listdir(out_dir):
        logger.debug('Loading variants')
        with open(out_dir + '_variants.pickle', 'r') as f:
            variants = pickle.load(f)
    else:
        logger.debug('Finding variants')
        variants = {}

        for vcf_file in list_of_files:
            if '1-0028' not in vcf_file and not vcf_file.startswith('A') and not vcf_file.startswith('H37'):
                vcffile = pysam.VariantFile(vcf_file)
                vcf = vcf_file.split('/')[-1]
                isolate = vcf.split('.')[0]
                # For each gene
                for gene in gene_drug_dict[input_drug]:
                    start = gene_dict[gene][0]
                    stop = gene_dict[gene][1]
                    for record in vcffile.fetch('1', start, stop):
                        ref_codon_var = variants_func(record, gene)
                        if not ref_codon_var:
                            continue
                        if isolate in variants.keys():
                            variants[isolate].append(ref_codon_var)
                        else:
                            variants[isolate] = [ref_codon_var]
                # If pysam fetch returned no variants
                if isolate not in variants.keys():
                    variants[isolate] = []
        with open(out_dir + '_variants.pickle', 'w') as f:
            pickle.dump(variants, f)
    return variants


def single_isolate(input_drug, list_of_files):
    variants = {}
    vcffile = pysam.VariantFile(list_of_files[0])
    isolate = list_of_files[0].split('/')[-1].split('.')[0]
    # For each gene
    for input_gene in gene_drug_dict[input_drug]:
        gene = input_gene
        start = gene_dict[input_gene][0]
        stop = gene_dict[input_gene][1]
        for record in vcffile.fetch('1', start, stop):
            ref_codon_var = variants_func(record, gene)
            if not ref_codon_var:
                continue

            if isolate in variants.keys():
                variants[isolate].append(ref_codon_var)
            else:
                variants[isolate] = [ref_codon_var]
    # If pysam fetch returned no variants
    if isolate not in variants:
        variants[isolate] = []
    return variants


def denovo(isolate, genes):
    gene_mutation = []
    try:
        for gene in genes:
            denovo_vcf = GROUPHOME + '/data/variants/mummer-denovo/' + isolate + '.vcf.gz'
            vcf = pysam.VariantFile(denovo_vcf)
            for record in vcf.fetch('1', gene_dict[gene][0], gene_dict[gene][1]):
                mutation = variants_func(record, gene)
                if not mutation:
                    continue
                gene_mutation.append(mutation)
    except IOError:
        gene_mutation = []
    return gene_mutation


def get_orientation(gene_name):
    """Gets the orientation ('+' or '-') of the input gene"""
    gene = gene_name.split('_')[0]
    genes_file = GROUPHOME + '/resources/mtb-reconstruction/genes.tsv'
    with open(genes_file, 'r') as genes:
        for line in genes:
            column = line.rstrip('\n').split('\t')
            if 'Rv' in gene and gene in column[0]:
                return column[5]
            elif 'Rv' not in gene and gene in column[1]:
                return column[5]


def variants_func(record, gene):
    """Retreives all mutations for genes involved in specified
        drug resistance for a single isolate."""

    string_consequences = ','.join(record.info['CSQ'])
    split_consequences = string_consequences.split(',')
    for consequence in split_consequences:
        conseq = consequence.split('|')
        if gene.split('_prom')[0] in conseq:
            orientation = get_orientation(gene.split('_prom')[0])

            if ('eis' in gene or 'RNA' in conseq[7] or 'pncA_prom' in gene) and orientation == '-':
                reference = Seq(record.ref).reverse_complement()[0]
                variant = Seq(''.join(record.alts)).reverse_complement()[0]
            else:
                reference = record.ref
                variant = ''.join(record.alts)
            # SNP
            if len(record.ref) == len(''.join(record.alts)):
                # Promoter
                if 'upstream' in conseq[1]:
                    ref = reference
                    var = variant
                    codon = '-' + conseq[18]
                    gene = conseq[3]
                    ref_codon_var = [gene, ref + codon + var]
                else:
                    amino_acid = conseq[15].split('/')
                    if len(amino_acid) > 1:
                        if len(amino_acid[0]) == 1:
                            ref = seq3(amino_acid[0])
                            var = seq3(amino_acid[1])
                            codon = conseq[14]
                            gene = conseq[3]
                            ref_codon_var = [gene, ref + codon + var]
                        else:
                            temp_ref = []
                            temp_alt = []
                            temp_codon = []
                            temp_final = []
                            for ref_aa in amino_acid[0]:
                                temp_ref.append(seq3(ref_aa))
                            for alt_aa in amino_acid[1]:
                                temp_alt.append(seq3(alt_aa))
                            for codon_aa in conseq[14].split('-'):
                                temp_codon.append(codon_aa)
                            for index in range(len(amino_acid[0])):
                                temp_final.append(temp_ref[index-1] + temp_codon[index - 1] + temp_alt[index - 1])
                            ref_codon_var = [gene, ','.join(temp_final)]
                    # RNA gene
                    elif '' in amino_acid:
                        ref = record.ref
                        var = ''.join(record.alts)
                        codon = conseq[12]
                        ref_codon_var = [gene, ref + codon + var]
                    # Synonymous change
                    elif len(amino_acid) == 1:
                        ref = seq3(amino_acid[0])
                        var = seq3(amino_acid[0])
                        codon = conseq[14]
                        ref_codon_var = [gene, ref + codon + var]
            # InDel
            elif len(record.ref) > 1 or len(''.join(record.alts)) > 1:
                if 'upstream' in conseq[1]:
                    # Promoter Region
                    combined = []
                    difference = list(Differ().compare(reference, variant))
                    edited = [base.replace(' ', '') for base in difference[1:]]
                    for i in edited[1:]:
                        if '+' in i:
                            combined.append(i.replace('+', ''))
                        elif '-' in i:
                            combined.append(i.replace('-', ''))
                        else:
                            combined.append(i)
                    edited[1:] = combined
                    ref = ''.join(edited)
                    var = ''
                    codon_column = conseq[18].split('-')
                    if len(codon_column) == 1:
                        codon = '-' + ''.join(codon_column)
                    else:
                        codon = '-' + codon_column[0]
                    ref_codon_var = [gene, ref + codon + var]
                elif 'inframe_deletion' in conseq[1]:
                    codon = conseq[14]
                    amino_acid = conseq[15].split('/')
                    ref = amino_acid[0]
                    var = amino_acid[1]
                    ref_codon_var = [gene, ref + codon + var]
                else:
                    if 'RNA' in conseq[7]:
                        codon_column = conseq[12].split('-')
                    else:
                        codon_column = conseq[13].split('-')
                    # Coding Region
                    combined = []
                    difference = list(Differ().compare(reference, variant))
                    edited = [base.replace(' ', '') for base in difference[1:]]
                    for i in edited[1:]:
                        if '+' in i:
                            combined.append(i.replace('+', ''))
                        elif '-' in i:
                            combined.append(i.replace('-', ''))
                        else:
                            combined.append(i)
                    edited[1:] = combined
                    ref = ''.join(edited)
                    var = ''
                    if len(codon_column) == 1:
                        codon = ''.join(codon_column)
                    else:
                        codon = codon_column[0]
                    ref_codon_var = [gene, ref + codon + var]
            return ref_codon_var


