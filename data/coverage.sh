#!/bin/bash

set -e

echo My argument is: $1
drug=$(echo $1)
echo $drug

### 1. create list of positions corresponding to known-mutations
positions=$(for i in $(awk '{ print $2 }' "$drug"/known-mutations ) ; do grep $i $GROUPHOME/data/vcp_results/gcdd.variants.txt | awk -F"\t" '{ print $3 }' | sort -u ; done )

### 2. get coverage information for list of known-genes for the unexplained r-isolates 
for j in $(ls "$drug"/unexplained/)
do 

cov=$(for i in $(cat "$drug"/unexplained/"$j" ) ; do for k in $(cat "$drug"/known-genes) ; do grep -w "$k" <(grep -w $i $GROUPHOME/data/vcp_results/gcdd.coverage.txt ) | awk -F"\t" '{OFS="\t"}{ if ($k ~ "Rv") print $1, $2, $3, $5; else print $1, $2, $3 }'  ;  done ; done)

### 3. print the coverage information - if there are positions of low coverage that match the list of positions (created from known-mutations) print those along with the % low coverage

#while read line ; do paste <(awk -F"\t" '{ print $1,$2,$3 }' <(echo "$line")) <( while IFS= read -r posns ; do if [[ $posns =~ "no_positions" ]] ; then echo "$posns" | awk '{ print $1 }'  ; else start=$(awk '{ print $1 }' <(echo "$posns") | grep -v '^$') ; stop=$(awk '{ print $2 }' <(echo "$posns") | grep -v '^$' ); re='^[0-9]+$' ; if [[ $start =~ $re ]] ; then grep -f <(echo "$positions") <(seq  $start $stop) ; fi ; fi | awk '$1=$1' ORS=', ' ;   done <<< "$( pos=$(awk -F"\t" '{ if (NF > 3) print $4 }'  <(echo "$line") | awk -v RS=',|\n' -F'-' '{OFS="\t"}{ if (NF == 2) {print $1,$2}  else {print $1, $1}}') ; echo "$pos"  )" ) | sed 's/, $/ /g'; done <<< "$cov"


while read line 
do 
paste <(awk -F"\t" '{ print $1,$2,$3 }' <(echo "$line")) <( while IFS= read -r posns 
do 
if [[ $posns =~ "no_positions" ]] 
then echo "$posns" | awk '{ print $1}'  
else start=$(awk '{ print $1 }' <(echo "$posns") | grep -v '^$') 
stop=$(awk '{ print $2 }' <(echo "$posns") | grep -v '^$' )
re='^[0-9]+$' 
if [[ $start =~ $re ]] 
then grep -f <(echo "$positions") <(seq  $start $stop)
fi 
fi | awk '$1=$1' ORS=', ' 
done <<< "$( pos=$(awk -F"\t" '{ if (NF > 3) print $4 }'  <(echo "$line") | awk -v RS=',|\n' -F'-' '{OFS="\t"}{ if (NF == 2) {print $1,$2}  else {print $1, $1}}') ; echo "$pos"  )" ) | sed 's/, $/ /g' # while IFS= read -r posns
done <<< "$cov"  >> "$drug"/unexplained/"$j"-coverage # while read line

done # for j in 
